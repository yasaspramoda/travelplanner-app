import React from "react";

class SearchSavedForm extends React.Component {
    constructor(props) {
        super(props);
        this.handleSearch = this.handleSearch.bind(this);
    }

    handleSearch(e) {
        e.preventDefault();
        let itName = e.target.itinerary.value;
        fetch(`http://localhost:8080/api/itinerary?name=${itName}`)
            .then(res => res.json())
            .then(
                (result) => {
                    result.forecasts.forEach(el => {
                        this.props.items[el.id] = el;
                    });
                    this.props.parentItemsCallback(this.props.items, true);
                },
                (error) => {
                    this.props.parentErrorCallback(error);
                }
            );
    }

    render() {
        return (
            <div>
                <form role="form" onSubmit={this.handleSearch}>
                    <div class="form-group"> 
                        <label for="Itinerary name"><h6>Enter saved itinerary name:</h6></label>
                        <input type="text" name="itinerary" placeholder="Itinerary name" required class="form-control "/>
                    </div>
                    <div class="card-footer"> 
                        <button class="subscribe btn btn-primary btn-block shadow-sm">Search</button>
                    </div>
                </form>
            </div>
        );
    }
}

export default SearchSavedForm;