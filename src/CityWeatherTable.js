import React from "react";

import SaveItineraryForm from './SaveItineraryForm'

class CityWeatherTable extends React.Component {
    constructor(props) {
        super(props);
        this.tableClear = this.tableClear.bind(this);
    }

    tableClear() {
        this.props.parentClearCallback()
    }

    render() {
        if (this.props.error && Object.entries(this.props.error).length !== 0) {
            return (
                <div>
                    Error occured! {this.props.error}
                </div>
            );
        } else if (Object.entries(this.props.items).length !== 0) {
            return (
                <div>
                    <SaveItineraryForm forecasts={Object.values(this.props.items)} 
                        isSearched={this.props.isSearched} 
                        parentTableClearCallback={this.handleTableClearCallback} />
                    <div class="col-sm-2">
                        <button class="btn" onClick={this.tableClear}>Clear</button>
                    </div>
                    <div class="table-responsive">
                        <table class="table table-responsive table-borderless">
                            <thead>
                                <tr>
                                    <th scope="col" width="5%">City</th>
                                    <th scope="col" width="5%">Country Code</th>
                                    <th scope="col" width="5%">Date &#38; Time</th>
                                    <th scope="col" width="5%">Temperature</th>
                                    <th scope="col" width="5%">Humidity</th>
                                    <th scope="col" width="5%">Rain</th>
                                    <th scope="col" width="5%">Clouds</th>
                                    <th scope="col" width="5%">Wind Speed</th>
                                </tr>
                            </thead>
                            {Object.entries(this.props.items).map(([id, item]) => (
                                <tr key={id}>
                                    <td>{item.city}</td>
                                    <td>{item.countryCode}</td>
                                    <td>{item.dateTime}</td>
                                    <td>{(item.temperature).toFixed(2)} &#8451;</td>
                                    <td>{item.humidity} &#37;</td>
                                    <td>{item.rain}</td>
                                    <td>{item.cloud} &#37;</td>
                                    <td>{item.windSpeed} kmh<sup>-1</sup></td>
                                </tr>
                            ))}
                        </table>
                    </div>
                </div>
            );
        } else {
            return (
                <div></div>
            );
        }
    }
}

export default CityWeatherTable;