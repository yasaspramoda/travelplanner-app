import React from "react";

class CityDateForm extends React.Component {
    constructor(props) {
        super(props);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    handleSubmit(e) {
        e.preventDefault();
        let city = e.target.city.value;
        let datetime = e.target.datetime.value;
        fetch(`http://localhost:8080/api/forecast?cityName=${city}&dateTime=${encodeURIComponent(datetime)}`)
            .then(res => res.json())
            .then(
                (result) => {
                    result.forEach(el => {
                        this.props.items[el.id] = el;
                    });
                    this.props.parentItemsCallback(this.props.items, false);
                },
                (error) => {
                    this.props.parentErrorCallback(error);
                }
            );
    }
    
    render() {
        return (
            <div>
                <form role="form" onSubmit={this.handleSubmit}>
                    <div class="form-group"> 
                        <label for="city">
                            <h6>Enter the city that you're planning to travel:</h6>
                        </label>
                        <input type="text" name="city" placeholder="City" required class="form-control "/>
                    </div>
                    <div class="form-group"> 
                        <label for="datetime">
                            <h6>Enter your arrivel date-time:</h6>
                        </label>
                        <input type="text" name="datetime" placeholder="yyyy-MM-dd HH:mm" required class="form-control "/>
                    </div>
                    <div class="card-footer"> 
                        <button class="subscribe btn btn-primary btn-block shadow-sm">Submit</button>
                    </div>
                </form>
            </div>
        );
    }
}

export default CityDateForm;