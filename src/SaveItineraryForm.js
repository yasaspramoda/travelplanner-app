import React from "react";

class SaveItineraryForm extends React.Component {
    constructor(props) {
        super(props);
        this.handleSave = this.handleSave.bind(this);
        this.state = {
            result: ""
        }
    }

    handleSave(e) {
        e.preventDefault();
        let itineraryName = e.target.itineraryName.value;
        if (this.props.forecasts.length !== 0) {
            let requestObj = {};
            requestObj[itineraryName] = this.props.forecasts;
            fetch('http://localhost:8080/api/itinerary', {
                method: 'POST',
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json',
                },
                body: JSON.stringify(requestObj)
            }).then(
                (rslt) => {
                    if (rslt.status === 200) {
                        this.setState({ result: "Successfully saved!" });
                    } else {
                        this.setState({ result: "Error saving itinerary!" });
                    }
                },
                (error) => {
                    this.setState({ result: "Error saving itinerary!" });
                }
            );
        }
    }

    render() {
        if (this.props.isSearched) {
            return (<div></div>);
        } else {
            return (
                <div class="mb-2 d-flex justify-content-between align-items-center">
                    <form role="form" onSubmit={this.handleSave}>
                        <div class="row">
                            <div class="col-sm-8">
                                <input type="text" name="itineraryName" class="form-control w-100" placeholder="Itinerary name to save..." required/>
                            </div>
                            <div class="col-sm-4">
                                <button class="btn-primary">Save</button>
                            </div>
                            <div class="col-sm-8">
                                {this.state.result}
                            </div>
                        </div>
                    </form>
                </div>
            );
        }
    }

}

export default SaveItineraryForm;