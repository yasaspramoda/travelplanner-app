import './App.css';

import React from "react";

import CityDateForm from './CityDateForm';
import CityWeatherTable from './CityWeatherTable';
import SearchSavedForm from './SearchSavedForm';

class App extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      searched: false,
      items: {},
      error: null
    };
  }

  handleItemsCallback = (childData, isSearched) => {
    this.setState({
      items: childData,
      searched: isSearched
    });
  }

  handleErrorCallback = (childError) => {
    this.setState({ error: childError });
  }

  handleClearCallback = () => {
    this.setState({
      items: {},
      searched: false
    });  
  }

  render() {
    return (
      <div id="viewport">
        <div id="sidebar">
          <div class="container py-5">
            <div class="row mb-4">
              <div class="col-lg-8 mx-auto text-center">
                <h1 class="display-6">Plan your travel</h1>
              </div>
            </div>
            <div class="row">
              <div class="col-lg-6 mx-auto">
                <div class="card ">
                  <div class="card-header">
                    <div class="bg-white shadow-sm pt-4 pl-2 pr-2 pb-2">
                      <ul role="tablist" class="nav bg-light nav-pills rounded nav-fill mb-3">
                        <li class="nav-item"> <a data-toggle="pill" href="#new" class="nav-link active "> New Itinerary </a> </li>
                        <li class="nav-item"> <a data-toggle="pill" href="#saved" class="nav-link "> Search Saved Itineraries </a> </li>
                      </ul>
                    </div>
                    <div class="tab-content">
                      <div id="new" class="tab-pane fade show active pt-3">
                        <CityDateForm
                          items={this.state.items}
                          parentItemsCallback={this.handleItemsCallback}
                          parentErrorCallback={this.handleErrorCallback} />
                      </div>
                      <div id="saved" class="tab-pane fade pt-3">
                        <SearchSavedForm
                          items={this.state.items}
                          parentItemsCallback={this.handleItemsCallback}
                          parentErrorCallback={this.handleErrorCallback} />
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div id="content">
          <div class="container-fluid">
            <div class="container mt-5 px-2">
              <div class="mb-2 d-flex justify-content-between align-items-center" />
              <CityWeatherTable
                items={this.state.items}
                error={this.state.error}
                isSearched={this.state.searched} 
                parentClearCallback={this.handleClearCallback} />
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default App;
